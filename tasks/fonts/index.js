'use strict';

var gulp = require('gulp'),
    common = require('../common');

/**
 * gulp fonts
 */

gulp.task('fonts', function() {
    gulp.src(common.fonts.src)
        .pipe(gulp.dest(common.fonts.dest));
});