'use strict';

var gulp = require('gulp'),
    plugins = require('gulp-load-plugins')(),
    common = require('../common');

/**
 * gulp styles
 */

gulp.task('styles', function() {
    gulp.src(common.styles.src)
        .pipe(plugins.plumber())
        .pipe(plugins.sourcemaps.init())
            .pipe(plugins.sass({ outputStyle: 'compact' }))
            .pipe(plugins.autoprefixer(common.styles.autoPrefixSettings))
        .pipe(plugins.sourcemaps.write())
        .pipe(gulp.dest(common.styles.dest));
});