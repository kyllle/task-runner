'use strict';

var gulp = require('gulp'),
    runSequence = require('run-sequence');

/**
 * gulp js-quality
 */

gulp.task('js-quality', function(cb) {
    runSequence('jscs', 'lint', cb);
});