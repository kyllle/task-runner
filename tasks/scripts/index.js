'use strict';

var gulp = require('gulp'),
    common = require('../common'),
    args = require('yargs').argv,
    webpack = require("webpack"),
    webpackConfig = require("../../webpack.config.js"),
    stream = require('webpack-stream');

/**
 * gulp scripts
 */

gulp.task('scripts', function() {

    if (args.hasOwnProperty('is-production') && args['is-production'] === true) {
        webpackConfig.plugins.push(
            new webpack.optimize.DedupePlugin()
        );

        webpackConfig.plugins.push(
            new webpack.optimize.UglifyJsPlugin({
                compress: {
                    warnings: false
                },

                sourceMap: false
            })
        );
    }

    gulp.src(common.scripts.src)
        .pipe(stream(webpackConfig))
        .pipe(gulp.dest(common.scripts.dest));
});