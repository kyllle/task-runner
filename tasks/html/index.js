'use strict';

var gulp = require('gulp'),
    common = require('../common');

/**
 * gulp html
 */

gulp.task('html', function() {

    gulp.src(common.html.src)
        .pipe(gulp.dest(common.html.dest));
});