'use strict';

var gulp = require('gulp'),
    plugins = require('gulp-load-plugins')(),
    common = require('../common'),
    pngquant = require('imagemin-pngquant');

/**
 * gulp images
 */

gulp.task('images', function() {
    gulp.src(common.images.src)
        .pipe(plugins.imagemin({
            progressive: true,
            svgoPlugins: [
                { removeViewBox: false },
                { removeUselessStrokeAndFill: false },
                { convertPathData: { straightCurves: false } },
                { cleanupIDs: false }
            ],
            use: [pngquant()]
        }))
        .pipe(gulp.dest(common.images.dest));
});