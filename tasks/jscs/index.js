'use strict';

var gulp = require('gulp'),
    plugins = require('gulp-load-plugins')(),
    common = require('../common'),
    chalk = require('chalk');

/**
 * gulp jscs
 */

gulp.task('jscs', function() {
    gulp.src(common.scripts.src)
        .pipe(plugins.debug({ title: 'JSCS:' }))
        .pipe(plugins.jscs())
        .on('error', function() {
            setTimeout(function() {
                console.log(chalk.bgRed.white("\n Fix errors then try to re-build."));
            }, 0);
        });
});