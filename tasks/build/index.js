var gulp = require('gulp'),
    plugins = require('gulp-load-plugins')(),
    runSequence = require('run-sequence'),
    del = require('del');

/**
 * gulp build
 *
 * @todo break out to have development and production builds
 */

gulp.task('build', function(cb) {
    runSequence('clean', 'html', 'images', 'styles', 'scripts', 'fonts', cb);
});