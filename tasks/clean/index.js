'use strict';

var gulp = require('gulp'),
    common = require('../common'),
    del = require('del');

/**
 * gulp clean
 */

gulp.task('clean', function() {
    return del(common.distPath);
});