'use strict';

var gulp = require('gulp'),
    plugins = require('gulp-load-plugins')(),
    common = require('../common'),
    chalk = require('chalk');

/**
 * gulp lint
 */

gulp.task('lint', function() {
    gulp.src(common.scripts.src)
        .pipe(plugins.jshint())
        .pipe(plugins.debug({ title: 'Lint:' }))
        .pipe(plugins.jshint.reporter('jshint-stylish'))
        .pipe(plugins.jshint.reporter('fail'))
        .on('error', function() {
            setTimeout(function() {
                console.log(chalk.bgRed.white("\n Fix errors then re-build."));
            }, 0);
        });
});