var gulp = require('gulp'),
    plugins = require('gulp-load-plugins')(),
    webpack = require('webpack'),
    WebpackDevServer = require('webpack-dev-server'),
    webpackConfig = require('../../webpack.config.js'),
    chalk = require('chalk');

gulp.task('server', function(cb) {

    // modify some webpack config options
    var myConfig = Object.create(webpackConfig);

    myConfig.devtool = "eval";
    myConfig.debug = true;

    // Start a webpack-dev-server
    new WebpackDevServer(webpack(myConfig), {
        publicPath: myConfig.output.publicPath,
        hot: true,
        inline: true,
        quiet: false,
        noInfo: true,
        stats: {
            assets: false,
            colors: true,
            version: false,
            hash: false,
            timings: false,
            chunks: false,
            chunkModules: false
        }
    }).listen(9999, 'localhost', function(err) {
        if (err) {
            console.log(chalk.bgRed.white('[webpack-dev-server] ERROR starting http://localhost:9999/webpack-dev-server/dist/html/'));
        }

        console.log(chalk.bgGreen.black('[webpack-dev-server] http://localhost:9999/webpack-dev-server/dist/html/'));
    });
});