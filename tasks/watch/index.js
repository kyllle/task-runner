var gulp = require('gulp'),
    common = require('../common');

gulp.task('watch', function() {

    // Watch .html files
    gulp.watch(common.html.watch, ['html']);

    // Watch .scss files
    gulp.watch(common.styles.watch, ['styles']);

    // Watch .js files
    gulp.watch(common.scripts.watch, ['scripts']);

    // Watch templates
    gulp.watch(common.templates.watch, ['scripts']);

    // Watch image files
    gulp.watch(common.images.watch, ['images']);

    // Watch font files
    gulp.watch(common.fonts.watch, ['fonts']);
});