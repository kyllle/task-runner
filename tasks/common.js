module.exports = {

    distPath: './dist',

    html: {
        watch: './app/html/*.html',
        src: './app/html/*',
        dest: './dist/html'
    },

    scripts: {
        watch: './app/scripts/**/*.js',
        src: ['./app/scripts/**/*.js', './app/scripts/**/*.jsx'],
        dest: './dist/scripts'
    },

    styles: {
        watch: './app/styles/**/*.scss',
        src: './app/styles/main.scss',
        dest: './dist/styles',
        autoPrefixSettings: {
            browsers: ['last 2 versions', 'iOS >= 7.1', 'Android >= 4'],
            cascade: false
        }
    },

    images: {
        watch: './app/images/**/*',
        src: './app/images/**/*',
        dest: './dist/images'
    },

    fonts: {
        watch: './app/fonts/**/*',
        src: './app/fonts/**/*',
        dest: './dist/fonts'
    },

    templates: {
        watch: './app/templates/**/*.html'
    },

    test: {
        path: './path/to/test/config'
    }
};