var path = require('path'),
    webpack = require('webpack');

module.exports = {
    entry: {
        bundle: ['./app/scripts/index.js', 'webpack/hot/only-dev-server'],
        devServer: 'webpack-dev-server/client?http://localhost:9999/'
    },
    output: {
        path: path.resolve(__dirname, 'dist/scripts/'),
        publicPath: '/dist/scripts/',
        filename: '[name].min.js'
    },
    resolveLoader: {
        'modulesDirectories': ['node_modules']
    },
    module: {
        loaders: [
            {
                test: /\.html$/,
                loader: 'underscore-template-loader'
            },
            {
                test: /\.js$/,
                exclude: /(node_modules|bower_components)/,
                loader: 'babel?presets[]=es2015'
            },
            {
                test: /\.jsx?$/,
                exclude: /(node_modules|bower_components)/,
                loader: 'babel?presets[]=es2015'
            },
            {
                test: /\.css$/,
                loader: 'style!css'
            },
            {
                test: /\.scss$/,
                loader: 'style!css!sass' // ['style', 'css', 'sass']
            }
        ]
    },
    plugins: [
        // Underscore needs to be made global in order to be used inside templates etc.
        new webpack.ProvidePlugin({
            _ : 'underscore'
        }),

        // https://goo.gl/EfdKMC
        new webpack.HotModuleReplacementPlugin(),

        // https://goo.gl/MjKYK4
        new webpack.optimize.DedupePlugin(),

        // https://goo.gl/5RHp7B
        new webpack.SourceMapDevToolPlugin({
            filename: '[name].min.js.map'
        }),

        new webpack.NoErrorsPlugin()
    ]
};
