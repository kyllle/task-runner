var Backbone = require('backbone'),
    Movie = require('./Movie.js');

module.exports = Backbone.Collection.extend({

    model: Movie

});
