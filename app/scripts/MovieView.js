'use strict';

define([
    'backbone',
    'underscore'
], function(
    Backbone,
    _
){
    return Backbone.View.extend({

        className: 'movie-grid',

        template: require('../templates/movies.html'),

        initialize: function() {
            _.each(this.collection.toJSON(), function(movie) {
                console.log(movie);
            });
        },

        render: function() {
            this.$el.html(this.template({
                movies: this.collection.toJSON()
            }));

            return this;
        }
    });
});
