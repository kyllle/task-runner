'use strict';

import MovieView from './MovieView.js';
import Movies from './Movies.js';
import FilmModel from './Movie.js';
import FilmView from './FilmView.js';

(() => {

    // Dummy Example code.

    var appEl = document.body.querySelector('.js-app');

    var data = [
        {
            "adult": false,
            "backdrop_path": "/kiWvoV78Cc3fUwkOHKzyBgVdrDD.jpg",
            "genre_ids": [18, 12],
            "id": 281957,
            "original_language": "en",
            "original_title": "The Revenant",
            "overview": "In the 1820s, a frontiersman, Hugh Glass, sets out on a path of vengeance against those who left him for dead after a bear mauling.",
            "release_date": "2015-12-25",
            "poster_path": "/oXUWEc5i3wYyFnL1Ycu8ppxxPvs.jpg",
            "popularity": 2.295314,
            "title": "The Revenant",
            "video": false,
            "vote_average": 5,
            "vote_count": 20
        },
        {
            "adult": false,
            "backdrop_path": "/pgSkCrrPO5NWZcvHpXG2l8DxTou.jpg",
            "genre_ids": [28, 12, 878, 14],
            "id": 140607,
            "original_language": "en",
            "original_title": "Star Wars: Episode VII - The Force Awakens",
            "overview": "A continuation of the saga created by George Lucas, set thirty years after Star Wars: Episode VI – Return of the Jedi.",
            "release_date": "2015-12-18",
            "poster_path": "/fYzpM9GmpBlIC893fNjoWCwE24H.jpg",
            "popularity": 12.066421,
            "title": "Star Wars: Episode VII - The Force Awakens",
            "video": false,
            "vote_average": 7.8,
            "vote_count": 115
        }
    ];

    var movies = new Movies(data);

    var movieView = new MovieView({
        collection: movies
    });

    var filmModel = new FilmModel({
        name: 'ABCDEFGH'
    });

    var filmView = new FilmView({
        model: filmModel
    });

    setTimeout(function() {
        filmModel.set('name', 'IJKLMNOPQRST');
    }, 5000);

    appEl.appendChild(movieView.render().el);
    appEl.appendChild(filmView.render().el);
})();