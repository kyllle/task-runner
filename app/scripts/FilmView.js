'use strict';

// https://goo.gl/hqFSjU && http://goo.gl/CdhCio

import * as Backbone from 'backbone';
import * as _ from 'underscore';

class FilmView extends Backbone.View {

    constructor(options) {
        super(options);

        this.template = _.template('<h1><%= name %></h1>');

        this.listenTo(this.model, 'change', this.render, this);
    }

    render() {
        this.$el.html(this.template(this.model.toJSON()));

        return this;
    }
}

export default FilmView;